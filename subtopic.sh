# https://gpgdigital.atlassian.net/wiki/spaces/FLO/pages/1431504396/Testing+with+the+MQTT+broker
set -x

usage() {
  echo "usage: $0 -d <device_id> -t <topic> [-h]"
}

while getopts ":t:d:h" arg; do
    case $arg in
        d)
            device_id=${OPTARG}
            ;;
        t)
            topic=${OPTARG}
            ;;
        h | *)
            usage
            exit
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${device_id}" ] || [ -z "${topic}" ]; then
    usage
    exit
fi

echo "device_id=${device_id}"
echo "topic=${topic}"

source ./init.sh
mqtttopic=home/device/$device_id/v1/$topic

mosquitto_sub -d -v -i $device_id -h $host -p $port -t "$mqtttopic" --cafile "$cafile" --cert "$cert" --key "$key"
