#!/usr/local/bin/bash
set -x

brokerCert='s3://flocloud-config/flo-apps/flo-ca/dev/flo-hivemq-keystore-with-ca-chain.cert.pem'

aws s3 cp $brokerCert .

