#!/usr/local/bin/bash

usage() {
  echo "usage: $0 -d <device_id>"
}

while getopts ":d:" arg; do
    case $arg in
        d)
            device_id=${OPTARG}
            ;;
        *)
            usage
            exit
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${device_id}" ]; then
    usage
    exit
fi

echo "device_id=${device_id}"

outfileCert=client-cert-$device_id.pem
outfileKey=client-key-$device_id.pem

deviceJson=$(aws dynamodb execute-statement --statement "SELECT * FROM \"dev_StockICD\".\"DeviceId\" WHERE device_id = '$device_id'")
# devicejson=$(aws dynamodb execute-statement --statement "SELECT * FROM \"prod_StockICD\".\"DeviceId\" WHERE device_id = '$device_id' --profile flo-prod")

# check for return items length
numitems=`echo "$deviceJson" | jq '.Items | length'`

if [ $numitems -lt 1 ]; then
    echo "*** cannot find StockICD records for device=$device_id  exiting..."
    exit 1
fi

echo $deviceJson | jq '.Items[0].icd_client_cert.S' | tr -d '"' | base64 -d | awk '/BEGIN CERTIFICATE/,/END CERTIFICATE/' > $outfileCert

echo $deviceJson | jq '.Items[0].icd_client_key.S' | tr -d '"' | base64 -d | awk '/BEGIN RSA PRIVATE KEY/,/END RSA PRIVATE KEY/' > $outfileKey



