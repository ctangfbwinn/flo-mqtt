# https://gpgdigital.atlassian.net/wiki/spaces/FLO/pages/1431504396/Testing+with+the+MQTT+broker
set -x

usage() {
  echo "usage: $0 -d <device_id> [-h]"
}

while getopts ":d:h" arg; do
    case $arg in
        d)
            device_id=${OPTARG}
            ;;
        h | *)
            usage
            exit
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${device_id}" ]; then
    usage
    exit
fi

echo "device_id=${device_id}"

source ./init.sh
topic=home/device/$device_id/v1/#
#topic=home/device/#

mosquitto_sub -d -v -i $device_id -h $host -p $port -t "$topic" --cafile "$cafile" --cert "$cert" --key "$key"
