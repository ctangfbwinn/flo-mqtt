# https://gpgdigital.atlassian.net/wiki/spaces/FLO/pages/1431504396/Testing+with+the+MQTT+broker
set -e

usage() {
    echo "usage: $0 -d <device_id> -t <topic> [-j <jq script>] [-h]"
}

while getopts ":t:d:j:hv" arg; do
    case $arg in
        d)
            device_id=${OPTARG}
            ;;
        t)
            topic=${OPTARG}
            ;;
        j)  
            jqscript=${OPTARG}
            ;;
        v)
            verbose=true
            ;;
        h | *)
            usage
            exit
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${device_id}" ] || [ -z "${topic}" ]; then
    usage
    exit
fi

echo "device_id=${device_id}"
echo "topic=${topic}"

source ./init.sh
mqtttopic=home/device/$device_id/v1/$topic
epoch=$(date +%s000)
id=$(uuidgen | tr '[:upper:]' '[:lower:]')

msg=$(cat ./topic-template/$topic.json | tr -d '\n')
msg=${msg//\$id/$id}
msg=${msg//\$device_id/$device_id}
msg=${msg//\$epoch/$epoch}

if [ -n "$jqscript" ]; then
    msg=`echo $msg | jq -c "$jqscript"`
fi

xtrace_set=${-//[^x]/}
if [ "$verbose" = "true" ]; then
    set -x
fi

mosquitto_pub -d -h $host -p $port -i $device_id -t "$mqtttopic" --cafile "$cafile" --cert "$cert" --key "$key" -m "$msg"

# if xtrace not set previously, then turn it off
if [ "xtrace_set" != "x" ]; then
    set +x
fi