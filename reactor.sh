#!/bin/bash

usage() {
  echo "usage: $0 -d <device_id> [-h]"
}

while getopts ":d:h" arg; do
    case $arg in
        d)
            device_id=${OPTARG}
            ;;
        h | *)
            usage
            exit
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${device_id}" ]; then
    usage
    exit
fi

echo "device_id=${device_id}"

source ./init.sh
# Topics to subscribe to
topic=home/device/$device_id/v1/#

# Function to publish a response
publish_response() {
    local topic=$1
    local jqscript=$2

    # if [ -n "$jqscript" ]; then
    #   jqarg="-j $jqscript"
    # fi

    sh pubtopic.sh -v -d $device_id -t $topic -j "$jqscript"
    # mosquitto_pub -h "$BROKER" -p "$PORT" --cafile "$CAFILE" --cert "$CERTFILE" --key "$KEYFILE" -t "$topic" -m "$message"
}

# send a telemetry message to make device online
publish_response telemetry

# Subscribe to topics and listen for messages
mosquitto_sub -v -i $device_id -h $host -p $port -t "$topic" --cafile "$cafile" --cert "$cert" --key "$key" | while read -r line
# mosquitto_sub -h "$BROKER" -p "$PORT" --cafile "$CAFILE" --cert "$CERTFILE" --key "$KEYFILE" -t "$TOPICS" | while read -r line
do
    # Extract the topic and message from the line
    # Assuming line format is "topic message"
    IFS=' ' read -r topic message <<< "$line"
    echo "Received topic=$topic, message=$message"

    # Determine the response based on the topic and/or message
    case "$topic" in
    "home/device/$device_id/v1/events/install/ack")
        # Example: Publish a response to a specific topic based on the received message
        publish_response $device_id "acknowledgement" ""
        ;;
    "home/device/$device_id/v1/properties/set")
        # Add more conditions and responses as needed
        ;;
    "home/device/$device_id/v1/directives")
        #{"id":"e39022e2-c7a0-4dd7-9769-695d8093df95","directive":"close-valve","device_id":"020000abcdef","time":"2024-02-12T23:51:35.719Z","ack_topic":"","data":{}}
        directive=`echo $message | jq -r '.directive'`
        case "$directive" in
        "close-valve")
            publish_response 'status/valve-state' '.pst=1 | .st=2'
            publish_response 'status/valve-state' '.pst=2 | .st=0'
            ;;
        "open-valve")
            publish_response 'status/valve-state' '.pst=0 | .st=2'
            publish_response 'status/valve-state' '.pst=2 | .st=1'
            ;;
        # "get-profile")
        #     ;;
        # "get-version")
        #     ;;
        # "get-pes-schedule")
        #     ;;
        # "get-health-test-config-v2")
        #     ;;
        # "power-reset")
        #     ;;
        # "factory-reset")
        #     ;;
        "set-system-mode")
            publish_response 'status/system-mode' ".st=$state"
            ;;
        *)
            echo "WARN: unhandled directive=$directive"
            ;;
        esac
        ;;
    *)
        # Default action for unspecified topics
        echo "WARN: unhandled $topic, $message"
        ;;
    esac
done
