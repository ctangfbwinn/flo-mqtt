# flo-mqtt

## Send MQTT messages to Flo
=====================================================

This project contains scripts to help setup and send mqtt messages to flo.  It is intended to serve as an example.
<br>
<b>Currently the scripts are pointing to the development environment only.</b>

## Prequisites
- have [mosquitto](https://mosquitto.org/) installed
- have access to flo-dev AWS environment and have aws-cli setup


## Usage

### Get the broker certificate - this is done once
*** needs access to flo-dev AWS environment and have aws-cli setup
```bash
$ cd certs
$ sh getBrokerCerts.sh
$ cd ..
```

### get the client certificates - done once per device
*** needs access to flo-dev AWS environment and have aws-cli setup
```bash
$ cd certs
$ sh getClientCerts.sh <device_id>
$ cd ..
```

### simulator to react to messages
```bash
$ sh reactor.sh -d <device_id>
```

### publish mqtt topic message
```bash
$ sh pubtopic.sh -d <device_id> -t <topic>
```
The list of supported topics can be found in `topic-template` folder

### subscribe to all mqtt topics for a device
```bash
$ sh suball.sh -d <device_id>
```

### subscribe to a specific mqtt topic for a device
```bash
$ sh subtopic.sh -d <device_id> -t <topic>
```

## Examples
### setup device `38d269deb0b7`:
```bash
$ cd certs
$ sh getClientCerts.sh -d 38d269deb0b7
$ cd ..
```
### run reactor:
```bash
$ sh reactor.sh -d 38d269deb0b7
```
### subscribe to all topics of a device:
```bash
$ sh suball.sh -d 38d269deb0b7
```
### update status/valve-state substituing .pst and .st values:
```bash
$ sh -x pubtopic.sh -d 020000abcdef -t status/valve-state -j '.pst=2 | .st=2'
```
### send to mqtt topic `home/device/38d269deb0b7/v1/alarm-notification-status`:
```bash
$ sh pubtopic.sh -d 38d269deb0b7 -t alarm-notification-status
```
### send to mqtt topic `home/device/38d269deb0b7/v1/status/system-mode`:
```bash
$ sh pubtopic.sh -d 38d269deb0b7 -t status/system-mode
```


