# https://gpgdigital.atlassian.net/wiki/spaces/FLO/pages/1431504396/Testing+with+the+MQTT+broker
# set -x

host=mqtt-dev.flocloud.co
port=8884
cafile=certs/flo-hivemq-keystore-with-ca-chain.cert.pem
cert=certs/client-cert-${device_id}.pem
key=certs/client-key-${device_id}.pem

